import { WebPOCPage } from './app.po';

describe('web-poc App', () => {
  let page: WebPOCPage;

  beforeEach(() => {
    page = new WebPOCPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
