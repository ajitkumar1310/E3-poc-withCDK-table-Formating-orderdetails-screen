import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DueorderDetailComponentComponent } from './dueorder-detail-component.component';

describe('DueorderDetailComponentComponent', () => {
  let component: DueorderDetailComponentComponent;
  let fixture: ComponentFixture<DueorderDetailComponentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DueorderDetailComponentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DueorderDetailComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
