import { Component, OnInit, NgZone, EventEmitter, Output, ViewChild, AfterViewInit } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { Headers, RequestOptions } from '@angular/http';
import { SlimLoadingBarService } from 'ng2-slim-loading-bar';
import { DataSource } from "@angular/cdk/table";
import { BehaviorSubject } from "rxjs/BehaviorSubject";
import { CollectionViewer } from "@angular/cdk/table";
import { DueOrderTableComponent } from '../OrdersComponent/due-order-table-component';


declare var FusionCharts: any;

@Component({
  selector: 'OrdersTable',
  templateUrl: './orders-component.component.html',
  styleUrls: ['./orders-component.component.css']
})
export class OrdersComponentComponent implements OnInit,AfterViewInit{
  chart1;
  chart2;
  options1: Object;
  options2: Object;
 
  new: number;
  pendingApproval: number;
  rejected: number;
  approved: number;
  isTableClicked = true;
  isspan = true;
  errorMessage: string;
  isOrdersChartClickedFirstTime:boolean=false;
  isExpediteClicked:boolean=false;
  ordersChartIndex1:number;
  private ordersChartUrl="http://e3cdd.jda.com:10000/web/services/AWR10/BUYERS/001?TYPE=DETAIL&ACTION=DETAIL_VIEW&003303=E3T&003174=JDA&3161=910";
  private dueOrdersListUrl="http://e3cdd.jda.com:10000/web/services/AWR10/ORDERS?TYPE=LIST&ACTION=LISTVIEW&PAGE=F&003303=E3T&003174=JDA&004261001=B&3161=910";
  private forwardBuysListUrl="http://e3cdd.jda.com:10000/web/services/AWR10/ORDERS?TYPE=LIST&ACTION=LISTVIEW&PAGE=F&003303=E3T&003174=JDA&&3161=910&004261001=C";
  private plannedListUrl="http://e3cdd.jda.com:10000/web/services/AWR10/ORDERS?TYPE=LIST&ACTION=LISTVIEW&PAGE=F&003303=E3T&003174=JDA&&3161=910&004261001=D";
  private bookedListUrl="http://e3cdd.jda.com:10000/web/services/AWR10/ORDERS?TYPE=LIST&ACTION=LISTVIEW&PAGE=F&003303=E3T&003174=JDA&&3161=910&004261001=E";
  private transferListUrl="http://e3cdd.jda.com:10000/web/services/AWR10/ORDERS?TYPE=LIST&ACTION=LISTVIEW&PAGE=F&003303=E3T&003174=JDA&3161=910&004261001=F";
  private backOrderListUrl="http://e3cdd.jda.com:10000/web/services/AWR10/ORDERS?TYPE=LIST&ACTION=LISTVIEW&PAGE=F&003303=E3T&003174=JDA&3161=910&004261001=G";
  private alterOrdersListUrl="http://e3cdd.jda.com:10000/web/services/AWR10/ORDERS?TYPE=LIST&ACTION=LISTVIEW&PAGE=F&003303=E3T&003174=JDA&&3161=910&004263001<=1&004261001<>Z";
  


  listName: string = '';
  isOrderChartClicked: boolean = false;
  data: any = null;
  listFilter: string;
  chartColumnClickedIndex:number;
  //dueOrderListDb = new DueOrderListDb();
  //dataSource: OrderDataSource | null;

  @ViewChild(DueOrderTableComponent)
  private dueOrderTableComponent: DueOrderTableComponent;
  constructor(private http: Http, private _ngZone: NgZone, private slimLoadingBarService: SlimLoadingBarService) {

    this.options1 = {
      title: { text: 'orders' },
      credits: {
                enabled: false
            },
      chart: {
        renderTo: 'chartContainer',
        type: 'column',
        spacingBottom: 3,
        //height: (screen.availHeight)-500,
        marginRight: 30,
        marginBottom: 50,
        height: 200,
        reflow: true
      },
      xAxis: { categories: ['Due', 'Alter', 'Forward', 'Planned', 'Booked', 'Transfers', 'Back'] },
      yAxis: {
        title: {
          text: 'Count'
        }
      },
      series: [{
        data: [30,20,10,40,10,50,5] //[{y:29.9,color: 'Yellow'}, {y:71.5,color:'green'}, 106.4, 129.2],
      }]
    };
    this.options2 = {
      title: { text: 'Expedite Orders' },
      credits: {
                enabled: false
            },
      chart: {
        renderTo: 'chartContainer2',
        type: 'column',
        spacingBottom: 3,
        //height: (screen.availHeight)-500,
        marginRight: 30,
        marginBottom: 50,
        height: 200,
        reflow: true
      },
      xAxis: { categories: ['Item Short Ship', 'Overdue Orders', 'Order Analysis Exceptions'] },   
      series: [{
        data: [30,20,10],
      }]
    };
  }

  columns = [
    { columnDef: 'Company', header: 'Company', cell: (row: IOrderData) => `${row.company}` },
    { columnDef: 'Warehouse', header: 'Warehouse', cell: (row: IOrderData) => `${row.warehouse}` },
    { columnDef: 'Vendor', header: 'Vendor', cell: (row: IOrderData) => `${row.vendor}` },
    { columnDef: 'SubVendor', header: 'SubVendor', cell: (row: IOrderData) => `${row.subvendor}` },
    { columnDef: 'Vendor Name', header: 'Vendor Name', cell: (row: IOrderData) => `${row.vendorname}` },
    { columnDef: 'Buyer Id', header: 'Buyer Id', cell: (row: IOrderData) => `${row.buyerid}` },
    { columnDef: 'Sup Vendor ID', header: 'Sup Vendor ID', cell: (row: IOrderData) => `${row.supvendor}` },
    { columnDef: 'Region', header: 'Region', cell: (row: IOrderData) => `${row.region}` },
    { columnDef: 'Amount', header: 'Amount', cell: (row: IOrderData) => `${row.amount}` },
    { columnDef: 'Eaches', header: 'Eaches', cell: (row: IOrderData) => `${row.eaches}` },
    { columnDef: 'Pallets', header: 'Pallets', cell: (row: IOrderData) => `${row.pallets}` }
  ];
  displayedColumns = this.columns.map(x => x.columnDef);

  intializechart() {
    //this.chart1.series[0].setData([29.9, 71.5, 106.4, 129.2]);
    /* this.getOrdersChartObservableDetails().subscribe((data:object[]) => this.onSuccessDueOrdersList(data),
            (error: any) => this.errorMessage = <any>error);
            console.log('order details Failed');*////commented for ram
  }
  onSuccessChartDetails(data) {
    console.log(data);
    console.log('successful got details');
  }
  onSuccessDueOrdersList(data) {
    console.log(data);
    console.log('successful got due orders list');
  }
  childTriggeredEvent(user) {
     if(this.isOrdersChartClickedFirstTime){
      this.isOrdersChartClickedFirstTime=false;
       this.dueOrderTableComponent.onOrdersChartClicked(this.chartColumnClickedIndex);
    }
    // Handle the event
  }
  
  onChartClick(e) {
    //this.orderChartBarChange.emit(e.point.x)

    this.isOrderChartClicked=true;
    if(this.dueOrderTableComponent)
      this.dueOrderTableComponent.onOrdersChartClicked(e.point.x);
    this.chartColumnClickedIndex=e.point.x;
    this.getListData(e.point.x);
    this.ordersChartIndex1=e.point.x;
  }

  ngAfterViewInit() {
    if(this.isOrdersChartClickedFirstTime){
      this.isOrdersChartClickedFirstTime=false;
       this.dueOrderTableComponent.onOrdersChartClicked(this.chartColumnClickedIndex);
    }
  }
  loadAnimationStart() {
    this.slimLoadingBarService.start();
  }

  loadAnimationStop() {
    this.slimLoadingBarService.complete();
  }


  getListData(value) {
    switch (value) {
      case 0://DueOrders
        // this.listName = 'Due Orders List';
        // this.loadAnimationStart();

        // this.dueOrdersList = [];

        // this.data = { "RRECD": "006000", "RSTAT": " ", "RCOMP": "E3T", "RWHSE": "920", "RVNDR": "0003    ", "RSUBV": "    ", "RWGRP": " ", "RTRUK": "000", "RSUPV": "SUPV    ", "RBUYR": "AK ", "RVNAME": "VENDOR 000003                 ", "RREGON": "910", "RSEQ#": "                  ", "ROTYP": "B", "RSTUS": "1", "RBRKT": "0", "RODEL": "000", "RTOT1F": "00000000002600000", "RTOT2F": "0000000260000", "RTOT3F": "0000000000000", "RTOT4F": "0000000260000", "RTOT5F": "0000000021666", "RTOT6F": "0000000260000", "RTOT7F": "0000000260000", "RTOT8F": "0000000260000", "RTOT9F": "0000000260000", "VGRP1": "   ", "VGRP2": "   ", "VGRP3": "   ", "VGRP4": "   ", "VGRP5": "   ", "VGRP6": "   ", "VOPTD3": "0", "VCURCD": "   ", "VMPRNT": "0", "RPINM": "0000000", "RXDRT": "006", "RGCNT": "00000", "VFCSTD": "       ", "VORMON": "00", "VORWEK": "0", "VNXTDT": "0000000", "WREGON": "   ", "RDLSTS": "0" };

        // let jsonData1 = this.data;
        // jsonData1["RTOT1F"] = Number.parseInt(jsonData1["RTOT1F"]);
        // jsonData1["RTOT2F"] = Number.parseInt(jsonData1["RTOT1F"]);
        // jsonData1["RTOT8F"] = Number.parseInt(jsonData1["RTOT1F"]);
        // this.dueOrdersList.push(jsonData1);
        //this.dataSource = new OrderDataSource(this.dueOrderListDb);
        // this._ngZone.run(() => { 'loading table data' });
        //this.isChartClicked = true;
        // this.loadAnimationStop();
        // console.log(this.data)
        break;

      case 1://DueOrders
        this.getDueOrdersListObservableDetails(this.dueOrdersListUrl).subscribe((data) => this.onSuccessChartDetails(data),
          (error: any) => this.errorMessage = <any>error);
        console.log('order details success');;
        break;
    }
  }

  saveInstance(chartInstance) {
    this.chart1 = chartInstance;
    this.intializechart();

  }

  togglespan() {
    this.isspan = !this.isspan;

  }

  selectData() {
    this.isTableClicked = true;
  }
  ngOnInit() {
   // this.dataSource = new OrderDataSource(this.dueOrderListDb);
  }
  
  getOrdersChartObservableDetails(): Observable<any> {
    let headers = new Headers({ 'Content-Type': 'application/json' });

    let options = new RequestOptions({ headers: headers });
    return this.http.get(this.ordersChartUrl, options)
      .map((res: Response) => {
        res = res.json();
        console.log(res);
        let jsonData = JSON.parse(res["JASONSTR"].toString());
        this.chart1.series[0].remove();
        let num: number = Number.parseInt(jsonData["BTMESG"])
        console.log(num);
        console.log(jsonData.BLTFHI);

        this.chart1.addSeries({name: 'Orders',data:[
                            Number.parseInt(jsonData["BORDUE"]),
                            Number.parseInt(jsonData["BALTSC"]),                            
                            Number.parseInt(jsonData["BORDFB"]),
                            Number.parseInt(jsonData["BORDPB"]),
                            Number.parseInt(jsonData["BMISC2"]),
                            Number.parseInt(jsonData["BMISC3"]),
                            Number.parseInt(jsonData["BORGCT"]),]}
        
        );
        this.chart2.series[0].remove();
              this.chart2.addSeries({name: 'Expedite',data:[
                            Number.parseInt(jsonData["BITMSS"]),
                            Number.parseInt(jsonData["BPLATE"]),                            
                            Number.parseInt(jsonData["BMISC8"]),]}
                             );
        console.log();

        console.log(res["NO_OF_ROWS"]);
        console.log(res["JASONSTR"]);
        console.log(res["JASONSTR"]["BRECD"]);

        console.log('getorders chart success');
      })
      .catch((error: any) => {
        return Observable.throw('server error')
      })
  }

  saveInstanceOrders(chartInstance) {
          this.chart1 = chartInstance;
      this.intializechart();
          
  }

  saveInstanceExpedite(chartInstance) {
          this.chart2 = chartInstance;
         
          
  }
    

  getDueOrdersListObservableDetails(url: string): Observable<any> {
    this.dueOrdersList = [];
    let headers = new Headers({ 'Content-Type': 'application/json' });

    let options = new RequestOptions({ headers: headers });
    return this.http.get(url, options)
      .map((res: Response) => {
        res = res.json();
        /*console.log(res); 
        let element=JSON.parse(res["JASONSTR"][0]);              
        for(let element of res["JASONSTR"]){              
          console.log(element);
          let jsonData = JSON.parse(element); 
          jsonData["RTOT1F"]=Number.parseInt(jsonData["RTOT1F"]);
          jsonData["RTOT2F"]=Number.parseInt(jsonData["RTOT1F"]);
          jsonData["RTOT8F"]=Number.parseInt(jsonData["RTOT1F"]);
          this.dueOrdersList.push(jsonData);              
                    
        }
        
        this._ngZone.run(()=>{'loading table data'});
        this.isChartClicked=true;
        console.log('getorders list success');*/
      })
      .catch((error: any) => {
        return Observable.throw('server error')
      })
  }
  dueOrdersList: object[] = [];
  jsonData = { "BRECD": "008401", "BSTAT": " ", "BBIRDT": "0000000", "BCOMP": "E3T", "BBUYR": "001", "BWHSE": "910", "BNAME": "DON O'BRYAN              ", "BBDAY": "0000000", "BUSRID": "DON       ", "BBROWS": "0", "BITMSS": "0000000", "BPLATE": "0000000", "BORDUE": "0000000", "BODDUE": "000000000000000", "BORPLC": "0000000", "BODPLC": "000000000000000", "BORDFB": "0000000", "BDLCHK": "0000000", "BIONDL": "0000000", "BFXDUE": "0000000", "BOPRIM": "0000000", "BOPREG": "0000000", "BUNINZ": "0000000", "BORDPB": "0000000", "BALTSC": "0000000", "BHSTEX": "0000000", "BMGTEX": "0000000", "BORDCT": "0000000", "BORECT": "0000000", "BORGCT": "0000000", "BORICT": "0000000", "BORKCT": "0000000", "BORLCT": "0000000", "BORRCT": "0000000", "BORSCT": "0000000", "BORTCT": "0000000", "BORVCT": "0000000", "BORWAT": "0000000", "BORXCT": "0000000", "BORZCT": "0000000", "BTMESG": "0000001", "BMMESG": "0000000", "BAMESG": "0000000", "BDMESG": "0000000", "BOPABD": "0000000", "BMISC1": "0000000", "BMISC2": "0000000", "BMISC3": "0000000", "BMISC4": "0000000", "BMISC5": "0000000", "BMISC6": "0000000", "BMISC7": "0000000", "BMISC8": "0000000", "BMISC9": "0000000", "BDHEXP": "0000000", "BDHDFH": "0000000", "BDHDFL": "0000000", "BDHTSH": "0000000", "BDHTSL": "0000000", "BDHSVC": "0000000", "BDHWAT": "0000000", "BDHMAN": "0000000", "BDHNEW": "0000000", "BDH1": "0000000", "BDH2": "0000000", "BDH3": "0000000", "BDH4": "0000000", "BLTEXP": "0000000", "BLTFHI": "0000076", "BLTFLW": "0000038", "BLTTSH": "0000000", "BLTTSL": "0000000", "BUSRDF": "                                                                      ", "BASISS": "0000000", "BASODO": "0000000", "BASORD": "0000000", "BASDUE": "000000000000000", "BASPLC": "0000000", "BASPLA": "000000000000000", "BASALT": "0000000", "BASBKO": "0000000", "BASDFB": "0000000", "BASDCT": "0000000", "BASECT": "0000000", "BASTFO": "0000000", "BASGCT": "0000000", "BASMOP": "0000000", "BASICT": "0000000", "BASCMC": "0000000", "BASKCT": "0000000", "BASLCT": "0000000", "BASMAN": "0000000", "BASODC": "0000000", "BASOPR": "0000000", "BASOPP": "0000000", "BASPLO": "0000000", "BASNEW": "0000000", "BASRCT": "0000000", "BASSCT": "0000000", "BASTCT": "0000000", "BASUNI": "0000000", "BASVCT": "0000000", "BASWAT": "0000000", "BASXCT": "0000000", "BASZCT": "0000000", "BASFOD": "0000000", "BASHST": "0000000", "BASMGT": "0000000", "BASEXC": "0000000", "BASOVR": "0000000", "BASOS": "0000000", "BASLST": "0000000", "BASOAE": "0000000", "BASALL": "0000000", "BASTMG": "0000000", "BASMMG": "0000000", "BASAMG": "0000000", "BASDMG": "0000000", "BASDMC": "0000000", "BASDFH": "0000000", "BASDFL": "0000000", "BASTSH": "0000000", "BASTSL": "0000000", "BASSVC": "0000000", "BASDH1": "0000000", "BASDH2": "0000000", "BASDH3": "0000000", "BASDH4": "0000000", "BASLTC": "0000000", "BASLFH": "0000000", "BASLFL": "0000000", "BASLTH": "0000000", "BASLTL": "0000000", "BASUSR": "                                                                      ", "WNAME": "                              " };

}

export interface IOrderData {
  company: string,
  warehouse: string,
  vendor: string,
  supvendor: string,
  vendorname: string,
  subvendor: string,
  buyerid: string,
  region: string,
  eaches: string,
  pallets: string,
  amount: string
}
export class OrderData implements IOrderData {
  company: string;
  warehouse: string;
  vendor: string;
  supvendor: string;
  vendorname: string;
  subvendor: string;
  buyerid: string;
  region: string;
  eaches: string;
  pallets: string;
  amount: string;
}
// Order Data source for Table
/*export class OrderDataSource extends DataSource<any> {
  connect(): Observable<any[]> {
    return this._dueOrderListDb.dataChange;
  }
  disconnect(collectionViewer: CollectionViewer): void {

  }
  constructor(private _dueOrderListDb: DueOrderListDb) {
    super();
  }
}

export class DueOrderListDb {
  dataChange: BehaviorSubject<OrderData[]> = new BehaviorSubject<OrderData[]>([]);
  get data(): OrderData[] { return this.dataChange.value }

  constructor() {
    let data = { "RRECD": "006000", "RSTAT": " ", "RCOMP": "E3T", "RWHSE": "920", "RVNDR": "0003    ", "RSUBV": "    ", "RWGRP": " ", "RTRUK": "000", "RSUPV": "SUPV    ", "RBUYR": "AK ", "RVNAME": "VENDOR 000003                 ", "RREGON": "910", "RSEQ#": "                  ", "ROTYP": "B", "RSTUS": "1", "RBRKT": "0", "RODEL": "000", "RTOT1F": "00000000002600000", "RTOT2F": "0000000260000", "RTOT3F": "0000000000000", "RTOT4F": "0000000260000", "RTOT5F": "0000000021666", "RTOT6F": "0000000260000", "RTOT7F": "0000000260000", "RTOT8F": "0000000260000", "RTOT9F": "0000000260000", "VGRP1": "   ", "VGRP2": "   ", "VGRP3": "   ", "VGRP4": "   ", "VGRP5": "   ", "VGRP6": "   ", "VOPTD3": "0", "VCURCD": "   ", "VMPRNT": "0", "RPINM": "0000000", "RXDRT": "006", "RGCNT": "00000", "VFCSTD": "       ", "VORMON": "00", "VORWEK": "0", "VNXTDT": "0000000", "WREGON": "   ", "RDLSTS": "0" };
    let dueOrdersList = [];
    let jsonData1: any = data;
    jsonData1["RTOT1F"] = Number.parseInt(jsonData1["RTOT1F"]);
    jsonData1["RTOT2F"] = Number.parseInt(jsonData1["RTOT1F"]);
    jsonData1["RTOT8F"] = Number.parseInt(jsonData1["RTOT1F"]);
    dueOrdersList.push(jsonData1);
    this.dataChange.next(this.createOrders(dueOrdersList));
  }

  private createOrders(orderList) {
    let orderData: OrderData[];
    orderList.forEach(element => {
      let tempOrder = new OrderData();
      tempOrder.warehouse = element["RWHSE"];
      tempOrder.company = element["RCOMP"];
      tempOrder.amount = element["RTOT1F"];
    });
    return orderData;
  }
}*/