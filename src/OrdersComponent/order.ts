export interface IOrder{
    RCOMP: string;
    RWHSE: string;
    RVNDR: string;
    RSUBV: string;
    RVNAME: number;
    RBUYR: string;
    RSUPV: number;
    RREGON: string;
     RTOT1F: string;
    RTOT2F: number;
    RTOT8F: string;
}