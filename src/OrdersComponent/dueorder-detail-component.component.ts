import { Component, OnInit } from '@angular/core';
import {Router, ActivatedRoute, Params} from '@angular/router';
import { AuthenticationService } from '../Services/authentication.service';
import { Headers, RequestOptions } from '@angular/http';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { DecimalPipe } from '@angular/common';

@Component({
  selector: 'app-dueorder-detail-component',
  templateUrl: './dueorder-detail-component.component.html',
  styleUrls: ['./dueorder-detail-component.component.css']
})
export class DueorderDetailComponent implements OnInit {


  isLoggedin:boolean;
  userName:string;
  appendQueryStr:string;
  dueOrderDetailsData:object[]=[];
  recieptDatesData;
  data:any=null;
  errorMessage: string;
  chart1;
  options1: Object;

  private dueOrdersDetailsUrl="http://e3cdd.jda.com:10000/web/services/AWR10/ORDERS/001?TYPE=DETAIL&ACTION=DETAIL_VIEW&";
  private recieptDatesUrl="http://e3cdd.jda.com:10000/web/services/AWR10/ORDERS/001?TYPE=DETAIL&ACTION=ORDDUEDAT&";
  private orderDetailsPostUrl="http://e3cdd.jda.com:10000/web/services/AWR10/ORDERS/001?TYPE=DTLACTION&ACTION=ACPBCH&003303=E3T&003174=AK";
  isOrderDatesLoaded:boolean=false;
  constructor(private _authservice:AuthenticationService,private router: Router,private activatedRoute: ActivatedRoute,private http: Http) { 
  
   this.isLoggedin=this._authservice.isLoggedIn();
    if( this.isLoggedin){
      this.userName=this._authservice.getUsername();
    }
    this.initializeChecksCharts();

  }

  ngOnInit() {
    this.activatedRoute.queryParams.subscribe((params: Params) => {
        let userId = params['whse'];
        let subVndr= params['subv'];
        let supVndr= params['supv'];
        this.appendQueryStr='003303=E3T&'+'003161'+'='+params['whse']+'&'+'004433'+'='+params['vndr'];
        if(subVndr)
          this.appendQueryStr=this.appendQueryStr+'&004435='+subVndr;
        if(supVndr)
          this.appendQueryStr=this.appendQueryStr+'&004439='+supVndr; 
        this.dueOrdersDetailsUrl=this.dueOrdersDetailsUrl+this.appendQueryStr;
        this.recieptDatesUrl=this.recieptDatesUrl+this.appendQueryStr+'&'+'004299=2014042';
        //this.dueOrdersDetailsUrl="http://e3cdd.jda.com:10000/web/services/AWR10/ORDERS/001?TYPE=DETAIL&ACTION=DETAIL_VIEW&003303=E3T&003174=001&003161=910&004433=5104";
        //this.recieptDatesUrl="http://e3cdd.jda.com:10000/web/services/AWR10/ORDERS/001?TYPE=DETAIL&ACTION=ORDDUEDAT&003303=E3T&003174=001&003161=920&004433=2329&004299=2014042";
        console.log(this.dueOrdersDetailsUrl);
      });
    this.getOrderDetails();  //commented for ram
    this.loadRecieptDates();
  }

  LogOut(){
    this._authservice.logout();
  }
  
  initializeChecksCharts(){
    this.options1 = {
            title : { text : 'Checks' },
            chart: {
            renderTo: 'chartContainer',
            type: 'bar',            
            spacingBottom: 3,
            //height: (screen.availHeight)-500,
            marginRight: 30,
            marginBottom: 50,
            //height:300,
            reflow: true
        },
        xAxis: { categories: ['Buyer Order Point Chk', 'Shelf Life Checks', 'Booking OP Items', 'OP Checks', 'Planning Items', 'Forward Buy Filter', 'Uninitialized','Event Items','Allocation Check','Insuff Qty Check','Constrained Checks'] },
        yAxis: {
                    title: {
                    text: 'Count'
                    }},
        series: [{
                data:[] //[{y:29.9,color: 'Yellow'}, {y:71.5,color:'green'}, 106.4, 129.2],
            }]
        
        };
    
  }
  saveInstance(chartInstance) {
      this.chart1 = chartInstance;
      this.chart1.series[0].remove()
      this.chart1.options.legend.enabled=false;
      this.chart1.addSeries({name: 'Orders',data:[10,20,30,40,50,60,70,80,90,100,110
                            /*Number.parseInt(this.dueOrderDetailsData["RHCNT"]),
                            Number.parseInt(this.dueOrderDetailsData["RICNT"]),                      
                            Number.parseInt(this.dueOrderDetailsData["RKCNT"]),
                            Number.parseInt(this.dueOrderDetailsData["ROCNT"]),
                            Number.parseInt(this.dueOrderDetailsData["RPCNT"]),
                            Number.parseInt(this.dueOrderDetailsData["RPLCNT"]),
                            Number.parseInt(this.dueOrderDetailsData["RTCNT"]),
                            Number.parseInt(this.dueOrderDetailsData["RUCNT"]),
                            Number.parseInt(this.dueOrderDetailsData["RVCNT"]),
                            Number.parseInt(this.dueOrderDetailsData["R0CNT"]),
                            Number.parseInt(this.dueOrderDetailsData["R2CNT"]),
                            Number.parseInt(this.dueOrderDetailsData["R3CNT"]),*/]}
                             );
          
      }
  loadRecieptDates(){
    let headers = new Headers({ 'Content-Type': 'application/json' });  
    let options = new RequestOptions({ headers: headers });
    this.isOrderDatesLoaded=true;
    this.data={"ODRECD":"006250","ODSTAT":" ","ODCOMP":"E3T","ODWHSE":"910","ODVNDR":"1050    ","ODSUBV":"    ","ODORDJ":"2014042","ODDUEJ":"2014043"};
    this.recieptDatesData=this.data;
    this.recieptDatesData["ODORDJ"]=this.julianToGregorian(this.recieptDatesData["ODORDJ"]);
    this.recieptDatesData["ODDUEJ"]=this.julianToGregorian(this.recieptDatesData["ODDUEJ"]);
    /*commented for Ram
    this.http.get(this.recieptDatesUrl,options).map(res=>res.json()).subscribe((data) => {   
        this.isOrderDatesLoaded=true;
        this.data=data.JASONSTR;
        this.recieptDatesData = JSON.parse(this.data); 
        //console.log(this.julianIntToDate(2456931));
       //this.recieptDatesData["ODORDJ"]=this.julianIntToDate(2456931);
        //console.log(this.recieptDatesData["ODORDJ"].toISOString().slice(0, 16));
        console.log(this.recieptDatesData["ODORDJ"]);
        let number=Date.parse(this.recieptDatesData["ODORDJ"]);
        console.log(this.julianToGregorian(this.recieptDatesData["ODORDJ"]));
        this.recieptDatesData["ODORDJ"]=this.julianToGregorian(this.recieptDatesData["ODORDJ"]);
        this.recieptDatesData["ODDUEJ"]=this.julianToGregorian(this.recieptDatesData["ODDUEJ"]);
        
        //console.log(this.julianIntToDate(number));
        //this.recieptDatesData["ODORDJ"]=this.convertStringToDate(this.recieptDatesData["ODORDJ"]);;
        
    //console.log(this.recieptDatesData["RRECD"]);
    //this.convertAmountToNumeric();
    },(error: any) => this.errorMessage = <any>error);
                  console.log('order details success');;*/
  }
  convertStringToDate(d){
    let date=new Date(d);
        let  mnth = ("0" + (date.getMonth()+1)).slice(-2);
        let  day  = ("0" + date.getDate()).slice(-2);
        return [  mnth, day,date.getFullYear() ].join("/");
  }

  

  getOrderDetails(){
    this.dueOrderDetailsData=[];
    this.data={"RRECD":"006001","RSTAT":" ","RCOMP":"E3T","RWHSE":"910","RVNDR":"1050    ","RSUBV":"    ","RWGRP":" ","RTRUK":"000","RSUPV":"        ","RBUYR":"JDA","RVNAME":"VENDOR 001050                 ","RREGON":"   ","RGOWH":"   ","RSEQ#":"                  ","ROTYP":"B","RPOTYP":"B","RORDUE":"1","RSTUS":"1","RPNUM":"0000000","RBRKT":"0","RODEL":"000","RRQDT":"0000000","RDLSTS":"0","RPRCN":"0","RXDRQ":"000","RXDRT":"000","RACNT":"00000","RBCNT":"00000","RCCNT":"00000","RDCNT":"00000","RECNT":"00000","RFCNT":"00000","RGCNT":"00000","RHCNT":"00000","RICNT":"00000","RJCNT":"00000","RKCNT":"00000","RLCNT":"00000","RMCNT":"00000","RNCNT":"00000","ROCNT":"00050","RPCNT":"00000","RPLCNT":"00000","RQCNT":"00000","RRCNT":"00000","RSCNT":"00000","RTCNT":"00000","RUCNT":"00001","RVCNT":"00000","RWCNT":"00000","RXCNT":"00000","RYCNT":"00000","RZCNT":"00000","R0CNT":"00000","R2CNT":"00086","R3CNT":"00000","R4CNT":"00000","RTOT1":"00000001110972000","RTOT2":"0000014380000","RTOT3":"0000000000000","RTOT4":"0000014380000","RTOT5":"0000001198337","RTOT6":"0000013930000","RTOT7":"0000013930000","RTOT8":"0000013930000","RTOT9":"0000014380000","RTOT1A":"00000001110972000","RTOT2A":"0000014380000","RTOT3A":"0000000000000","RTOT4A":"0000014380000","RTOT5A":"0000001198337","RTOT6A":"0000013930000","RTOT7A":"0000013930000","RTOT8A":"0000013930000","RTOT9A":"0000014380000","RTOT1F":"00000001110972000","RTOT2F":"0000014380000","RTOT3F":"0000000000000","RTOT4F":"0000014380000","RTOT5F":"0000001198337","RTOT6F":"0000013930000","RTOT7F":"0000013930000","RTOT8F":"0000013930000","RTOT9F":"0000014380000","RSSO":"00000000010039686","RESO1":"00000000927898673","RESO2":"00000000992641156","RESO3":"00000001057383639","RESO4":"00000001122126122","RESO5":"00000001186868605","RESO6":"00000001251611088","UNTRK":"0000","RDTRK":"000","TKSPU":"0","TKSPT":" ","TKSPM":"0000000000000","ERRM1":"        ","ERRV1":"000","ERRM2":"        ","ERRV2":"000","LTDLY":"999","RCRPL":"1","RCFWD":"1","RCPLN":"1","RCNSK":"0","RCNSD":"0000000","RCXFR":"1","RCBK":"1","RCBKD":"0000000","REPRJ":"1","INCDP":"0","VOPTD3":"0","VCURCD":"   ","VMPRNT":"1","RPINM#":"0000000","MPOREF":"          ","MPOGRP":"   ","MOTYPE":" "};
    this.dueOrderDetailsData = this.data; 
    this.convertAmountToNumeric();
    /*commented for ram
    let headers = new Headers({ 'Content-Type': 'application/json' });  
    let options = new RequestOptions({ headers: headers });
    this.http.get(this.dueOrdersDetailsUrl,options).map(res=>res.json()).subscribe((data) => {   
    this.data=data.JASONSTR;
    this.dueOrderDetailsData = JSON.parse(this.data); 
    
    //console.log(this.dueOrderDetailsData["RRECD"]);
    this.convertAmountToNumeric();
    },(error: any) => this.errorMessage = <any>error);
                  console.log('order details success');;*/
  }

  gregorianToJulian(n1){
    let monthDayDate:string;
    let year:number=n1.slice(0,4);
    let mm:number=n1.slice(5,7);
    let dd:number=n1.slice(8,10);
    let convertedDays;
    if(year%4==0){

      if(mm==2){
        convertedDays=Number.parseInt((mm-1).toString())*28+Number.parseInt(dd.toString());
      }else if(mm==1||mm==3||mm==5||mm==7||mm==8||mm==10||mm==12){
        convertedDays=Number.parseInt((mm-1).toString())*31+Number.parseInt(dd.toString());
      }else{
        convertedDays=Number.parseInt((mm-1).toString())*30+Number.parseInt(dd.toString());;
      }
        
    }else{
      if(mm==2){
        convertedDays=Number.parseInt((mm-1).toString())*29+Number.parseInt(dd.toString());
      }else if(mm==1||mm==3||mm==5||mm==7||mm==8||mm==10||mm==12){
        convertedDays=Number.parseInt((mm-1).toString())*31+Number.parseInt(dd.toString());
      }else{
        convertedDays=Number.parseInt((mm-1).toString())*30+Number.parseInt(dd.toString());
      }

    }
    if(convertedDays<100)
      return (year+'0'+convertedDays);
    else{
      return (year+convertedDays);
    }
  }

  julianToGregorian(n1) {
    let monthDayDate:string;
    let year:number=n1.slice(0,4);
    let mmdd:number=n1.slice(4);
    let n:number=mmdd;
    if(year%4==0){

        if(n>0&&n<=31)
            monthDayDate='01/'+n;
        if(n>31&&n<=60)
            monthDayDate='02/'+(n-31);
        if(n>60&&n<=91)
            monthDayDate='03/'+(n-60);
        if(n>91&&n<=121)
            monthDayDate='04/'+(n-91);
        if(n>121&&n<=152)
            monthDayDate='05/'+(n-121);
        if(n>152&&n<=182)
            monthDayDate='06/'+(n-152);
        if(n>182&&n<=213)  
            monthDayDate='07/'+(n-182);
        if(n>213&&n<=244)
            monthDayDate='08/'+(n-213);
        if(n>244&&n<=274)
            monthDayDate='09/'+(n-244);
        if(n>274&&n<=305)
            monthDayDate='10/'+(n-274);
        if(n>305&&n<=335)
            monthDayDate='11/'+(n-305);
        if(n>335&&n<=366)
            monthDayDate='12/'+(n-335);     
        
      
        }
    else{
        if(n>0&&n<=31)
            monthDayDate='01/'+n;
        if(n>31&&n<=59)
            monthDayDate='02/'+(n-31);
        if(n>59&&n<=90)
            monthDayDate='03/'+(n-60);
        if(n>90&&n<=120)
            monthDayDate='04/'+(n-91);
        if(n>120&&n<=151)
            monthDayDate='05/'+(n-121);
        if(n>151&&n<=181)
            monthDayDate='06/'+(n-152);
        if(n>181&&n<=211)  
            monthDayDate='07/'+(n-182);
        if(n>211&&n<=243)
            monthDayDate='08/'+(n-213);
        if(n>243&&n<=273)
            monthDayDate='09/'+(n-244);
        if(n>273&&n<=304)
            monthDayDate='10/'+(n-274);
        if(n>304&&n<=334)
            monthDayDate='11/'+(n-305);
        if(n>334&&n<=365)
            monthDayDate='12/'+(n-335);   

    }
        return (year+'/'+monthDayDate);
    

  }

 onClickCreateImmediate(){
   console.log('button clicked');
    let headers = new Headers({ 'Content-Type': 'application/json' });   
       
    let options = new RequestOptions({ headers: headers });
  
    this.dueOrderDetailsData["ODORDJ"]=this.gregorianToJulian(this.recieptDatesData["ODORDJ"]);;
    this.dueOrderDetailsData["ODDUEJ"]=this.gregorianToJulian(this.recieptDatesData["ODDUEJ"]); 
    
    this.http.post(this.orderDetailsPostUrl,JSON.stringify(this.dueOrderDetailsData), options)
                   .map(this.extractData).subscribe((data) => {   
                          
                  console.log(this.data);
                  this.router.navigate(['home']);
                },(error: any) =>
                 {
                   this.errorMessage = <any>error
                   console.log(this.errorMessage);
                  });;
    }

 private extractData(response: Response) {
        console.log('sucess');        
        return {};
    }

// assert 2456931 -> September 30 2014

  cols:Object[]=[
          
              {field: 'Independent', header: 'Independent'},
              {field:'Auto Adj.', header:'Auto Adj.'},
              {field: 'Final Adj.', header: 'Final Adj.'},
              {field: 'Totals', header: 'Totals'}            
              
          ];
  
  convertAmountToNumeric(){
    this.dueOrderDetailsData["RTOT1"]=Number.parseInt(String(this.dueOrderDetailsData["RTOT1"]).slice(0,-4));
    this.dueOrderDetailsData["RTOT2"]=Number.parseInt(String(this.dueOrderDetailsData["RTOT2"]).slice(0,-4));
    this.dueOrderDetailsData["RTOT3"]=Number.parseInt(String(this.dueOrderDetailsData["RTOT3"]).slice(0,-4));
    this.dueOrderDetailsData["RTOT4"]=Number.parseInt(String(this.dueOrderDetailsData["RTOT4"]).slice(0,-4));
    this.dueOrderDetailsData["RTOT5"]=Number.parseInt(String(this.dueOrderDetailsData["RTOT5"]).slice(0,-4));
    this.dueOrderDetailsData["RTOT6"]=Number.parseInt(String(this.dueOrderDetailsData["RTOT6"]).slice(0,-4));
    this.dueOrderDetailsData["RTOT7"]=Number.parseInt(String(this.dueOrderDetailsData["RTOT7"]).slice(0,-4));
    this.dueOrderDetailsData["RTOT8"]=Number.parseInt(String(this.dueOrderDetailsData["RTOT8"]).slice(0,-4));
    this.dueOrderDetailsData["RTOT9"]=Number.parseInt(String(this.dueOrderDetailsData["RTOT9"]).slice(0,-4));

    this.dueOrderDetailsData["RTOT1A"]=Number.parseInt(String(this.dueOrderDetailsData["RTOT1A"]).slice(0,-4));
    this.dueOrderDetailsData["RTOT2A"]=Number.parseInt(String(this.dueOrderDetailsData["RTOT2A"]).slice(0,-4));
    this.dueOrderDetailsData["RTOT3A"]=Number.parseInt(String(this.dueOrderDetailsData["RTOT3A"]).slice(0,-4));
    this.dueOrderDetailsData["RTOT4A"]=Number.parseInt(String(this.dueOrderDetailsData["RTOT4A"]).slice(0,-4));
    this.dueOrderDetailsData["RTOT5A"]=Number.parseInt(String(this.dueOrderDetailsData["RTOT5A"]).slice(0,-4));
    this.dueOrderDetailsData["RTOT6A"]=Number.parseInt(String(this.dueOrderDetailsData["RTOT6A"]).slice(0,-4));
    this.dueOrderDetailsData["RTOT7A"]=Number.parseInt(String(this.dueOrderDetailsData["RTOT7A"]).slice(0,-4));
    this.dueOrderDetailsData["RTOT8A"]=Number.parseInt(String(this.dueOrderDetailsData["RTOT8A"]).slice(0,-4));
    this.dueOrderDetailsData["RTOT9A"]=Number.parseInt(String(this.dueOrderDetailsData["RTOT9A"]).slice(0,-4));

    this.dueOrderDetailsData["RTOT1F"]=Number.parseInt(String(this.dueOrderDetailsData["RTOT1F"]).slice(0,-4));
    this.dueOrderDetailsData["RTOT2F"]=Number.parseInt(String(this.dueOrderDetailsData["RTOT2F"]).slice(0,-4));
    this.dueOrderDetailsData["RTOT3F"]=Number.parseInt(String(this.dueOrderDetailsData["RTOT3F"]).slice(0,-4));
    this.dueOrderDetailsData["RTOT4F"]=Number.parseInt(String(this.dueOrderDetailsData["RTOT4F"]).slice(0,-4));
    this.dueOrderDetailsData["RTOT5F"]=Number.parseInt(String(this.dueOrderDetailsData["RTOT5F"]).slice(0,-4));
    this.dueOrderDetailsData["RTOT6F"]=Number.parseInt(String(this.dueOrderDetailsData["RTOT6F"]).slice(0,-4));
    this.dueOrderDetailsData["RTOT7F"]=Number.parseInt(String(this.dueOrderDetailsData["RTOT7F"]).slice(0,-4));
    this.dueOrderDetailsData["RTOT8F"]=Number.parseInt(String(this.dueOrderDetailsData["RTOT8F"]).slice(0,-4));
    this.dueOrderDetailsData["RTOT9F"]=Number.parseInt(String(this.dueOrderDetailsData["RTOT9F"]).slice(0,-4));
  }

}
