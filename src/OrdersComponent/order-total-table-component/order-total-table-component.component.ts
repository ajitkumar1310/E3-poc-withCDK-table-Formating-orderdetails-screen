import { Component, ElementRef, ViewChild, Input, OnInit } from '@angular/core';
import { DataSource } from '@angular/cdk/table';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Observable } from 'rxjs/Observable';
import { Http, Response } from '@angular/http';
import { Headers, RequestOptions } from '@angular/http';
import { Router, ActivatedRoute, Params} from '@angular/router';


@Component({
  selector: 'order-totals-table',
  templateUrl: './order-total-table-component.component.html',
  styleUrls: ['./order-total-table-component.component.css']
})
export class OrderTotalTableComponentComponent  {

  orderTotalsListDb = new OrderTotalsListDb(this.router,this.activatedRoute,this._http);
  dataSource: OrderTotalsSource | null;
  ordersChartClicked:boolean=false;

  @Input() orderChartIndex:number;
  @ViewChild('filter') filter: ElementRef;

  /** Table columns */
  columns = [
    { columnDef: 'Independent', header: 'Independent', cell: (row: IOrderTableData) => `${row.independent}` },
    { columnDef: 'Auto Adj.', header: 'Auto Adjust', cell: (row: IOrderTableData) => `${row.autoadj}` },
    { columnDef: 'Final Adj.', header: 'Final Adj.', cell: (row: IOrderTableData) => `${row.finaladj}` },
    { columnDef: 'Totals', header: 'Totals', cell: (row: IOrderTableData) => `${row.totals}` },
      
  ];

  
  /** Column definitions in order */
 
  displayedColumns = this.columns.map(x => x.columnDef);
  
  handleRowClick(row){
    alert(row);
    let supvndr:string=row.supvendor;
    let subvndr:string=row.subvendor;
    let vndr:string=row.vendor;
    let whse:string=row.warehouse;
      console.log(supvndr.trim().length);
    this.router.navigate(['/dueorders'], { queryParams: { whse:whse,vndr:vndr.toString().trim(),supv:supvndr.trim(),subv:subvndr.trim() } });
  }

  OrdersChartClicked(event){
    console.log('chart clicked'+'  '+event);
  }

  onOrdersChartClicked(n:number) {
    console.log('Event Fired'+' '+n);
    
    this.orderTotalsListDb.addOrder();
    this.ordersChartClicked=true;
    
  }
   constructor(private _http: Http,private router: Router,private activatedRoute: ActivatedRoute,private http: Http) {
    //this.addOrder();
  }

  ngOnInit() {
    this.dataSource = new OrderTotalsSource(this.orderTotalsListDb);

    
  }
}
export interface IOrderTableData {
  independent: number,
  autoadj: number,
  finaladj: number,
  totals: string
  
}
export class OrderTableData implements IOrderTableData {
  independent: number;
  autoadj: number;
  finaladj: number;
  totals: string;
  
}
// Order Data source for Table
export class OrderTotalsSource extends DataSource<any> {
  _filterChange = new BehaviorSubject('');
  get filter(): string { return this._filterChange.value; }
  set filter(filter: string) { this._filterChange.next(filter); }
  connect(): Observable<any[]> {
    const displayDataChanges = [
      this._orderTableListDb.dataChange,
      this._filterChange,
    ];
    return Observable.merge(...displayDataChanges).map(() => {
      return this._orderTableListDb.data.slice().filter((item: OrderTableData) => {
        let searchStr = (item.totals).toLowerCase();
        return searchStr.indexOf(this.filter.toLowerCase()) != -1;
      });
    });
  }
  disconnect(): void {

  }
  constructor(private _orderTableListDb: OrderTotalsListDb) {
    super();
  }
}

export class OrderTotalsListDb implements OnInit{
  @Input() ordersChartIndex:number;
  @Input() expediteChartIndex:number;
  dueOrderDetailsData;
  appendQueryStr:string
  private dueOrdersDetailsUrl="http://e3cdd.jda.com:10000/web/services/AWR10/ORDERS/001?TYPE=DETAIL&ACTION=DETAIL_VIEW&";
  
  data1:any=null;
  errorMessage:string;
  dataChange: BehaviorSubject<OrderTableData[]> = new BehaviorSubject<OrderTableData[]>([]);
  get data(): OrderTableData[] { return this.dataChange.value }
  
  constructor(private router: Router,private activatedRoute: ActivatedRoute,private http: Http) {
    this.activatedRoute.queryParams.subscribe((params: Params) => {
        let userId = params['whse'];
        let subVndr= params['subv'];
        let supVndr= params['supv'];
        this.appendQueryStr='003303=E3T&'+'003161'+'='+params['whse']+'&'+'004433'+'='+params['vndr'];
        if(subVndr)
          this.appendQueryStr=this.appendQueryStr+'&004435='+subVndr;
        if(supVndr)
          this.appendQueryStr=this.appendQueryStr+'&004439='+supVndr; 
        this.dueOrdersDetailsUrl=this.dueOrdersDetailsUrl+this.appendQueryStr;
    });
    
    this.addOrder();
  }
  ngOnInit() {
    
  }
  
  public addOrder() {
    this.dueOrderDetailsData=[];
    let dueOrdersList = [];
    this.dueOrderDetailsData={"RRECD":"006001","RSTAT":" ","RCOMP":"E3T","RWHSE":"910","RVNDR":"1050    ","RSUBV":"    ","RWGRP":" ","RTRUK":"000","RSUPV":"        ","RBUYR":"JDA","RVNAME":"VENDOR 001050                 ","RREGON":"   ","RGOWH":"   ","RSEQ#":"                  ","ROTYP":"B","RPOTYP":"B","RORDUE":"1","RSTUS":"1","RPNUM":"0000000","RBRKT":"0","RODEL":"000","RRQDT":"0000000","RDLSTS":"0","RPRCN":"0","RXDRQ":"000","RXDRT":"000","RACNT":"00000","RBCNT":"00000","RCCNT":"00000","RDCNT":"00000","RECNT":"00000","RFCNT":"00000","RGCNT":"00000","RHCNT":"00000","RICNT":"00000","RJCNT":"00000","RKCNT":"00000","RLCNT":"00000","RMCNT":"00000","RNCNT":"00000","ROCNT":"00050","RPCNT":"00000","RPLCNT":"00000","RQCNT":"00000","RRCNT":"00000","RSCNT":"00000","RTCNT":"00000","RUCNT":"00001","RVCNT":"00000","RWCNT":"00000","RXCNT":"00000","RYCNT":"00000","RZCNT":"00000","R0CNT":"00000","R2CNT":"00086","R3CNT":"00000","R4CNT":"00000","RTOT1":"00000001110972000","RTOT2":"0000014380000","RTOT3":"0000000000000","RTOT4":"0000014380000","RTOT5":"0000001198337","RTOT6":"0000013930000","RTOT7":"0000013930000","RTOT8":"0000013930000","RTOT9":"0000014380000","RTOT1A":"00000001110972000","RTOT2A":"0000014380000","RTOT3A":"0000000000000","RTOT4A":"0000014380000","RTOT5A":"0000001198337","RTOT6A":"0000013930000","RTOT7A":"0000013930000","RTOT8A":"0000013930000","RTOT9A":"0000014380000","RTOT1F":"00000001110972000","RTOT2F":"0000014380000","RTOT3F":"0000000000000","RTOT4F":"0000014380000","RTOT5F":"0000001198337","RTOT6F":"0000013930000","RTOT7F":"0000013930000","RTOT8F":"0000013930000","RTOT9F":"0000014380000","RSSO":"00000000010039686","RESO1":"00000000927898673","RESO2":"00000000992641156","RESO3":"00000001057383639","RESO4":"00000001122126122","RESO5":"00000001186868605","RESO6":"00000001251611088","UNTRK":"0000","RDTRK":"000","TKSPU":"0","TKSPT":" ","TKSPM":"0000000000000","ERRM1":"        ","ERRV1":"000","ERRM2":"        ","ERRV2":"000","LTDLY":"999","RCRPL":"1","RCFWD":"1","RCPLN":"1","RCNSK":"0","RCNSD":"0000000","RCXFR":"1","RCBK":"1","RCBKD":"0000000","REPRJ":"1","INCDP":"0","VOPTD3":"0","VCURCD":"   ","VMPRNT":"1","RPINM#":"0000000","MPOREF":"          ","MPOGRP":"   ","MOTYPE":" "};
    this.convertAmountToNumeric();
     
    dueOrdersList.push(this.createOrderTotals1(this.dueOrderDetailsData)); 
    dueOrdersList.push(this.createOrderTotals2(this.dueOrderDetailsData));        
    dueOrdersList.push(this.createOrderTotals3(this.dueOrderDetailsData));  
    dueOrdersList.push(this.createOrderTotals4(this.dueOrderDetailsData));  
    dueOrdersList.push(this.createOrderTotals5(this.dueOrderDetailsData));  
    dueOrdersList.push(this.createOrderTotals6(this.dueOrderDetailsData));  
    dueOrdersList.push(this.createOrderTotals7(this.dueOrderDetailsData));  
    dueOrdersList.push(this.createOrderTotals8(this.dueOrderDetailsData));  
    dueOrdersList.push(this.createOrderTotals9(this.dueOrderDetailsData)); 
    this.dataChange.next(dueOrdersList);
    /*
    let headers = new Headers({ 'Content-Type': 'application/json' });  
    let options = new RequestOptions({ headers: headers });
    this.http.get(this.dueOrdersDetailsUrl,options).map(res=>res.json()).subscribe((data) => {  
    this.data1=data.JASONSTR;
    this.dueOrderDetailsData=JSON.parse(this.data1);     
    this.convertAmountToNumeric();
     
    dueOrdersList.push(this.createOrderTotals1(this.dueOrderDetailsData)); 
    dueOrdersList.push(this.createOrderTotals2(this.dueOrderDetailsData));        
    dueOrdersList.push(this.createOrderTotals3(this.dueOrderDetailsData));  
    dueOrdersList.push(this.createOrderTotals4(this.dueOrderDetailsData));  
    dueOrdersList.push(this.createOrderTotals5(this.dueOrderDetailsData));  
    dueOrdersList.push(this.createOrderTotals6(this.dueOrderDetailsData));  
    dueOrdersList.push(this.createOrderTotals7(this.dueOrderDetailsData));  
    dueOrdersList.push(this.createOrderTotals8(this.dueOrderDetailsData));  
    dueOrdersList.push(this.createOrderTotals9(this.dueOrderDetailsData));  

    this.dataChange.next(dueOrdersList);
    },(error: any) => this.errorMessage = <any>error);*/
      
    }

    
    private createOrderTotals1(element) {
    return {
      "independent": element["RTOT1"],
      "autoadj": element["RTOT1A"],
      "finaladj": element["RTOT1F"],
      "totals":'Amount',
      
    }
    }
    private createOrderTotals2(element) {
    return {
      "independent": element["RTOT2"],
      "autoadj": element["RTOT2A"],
      "finaladj": element["RTOT2F"],
      "totals":'Eaches',
      
    }
  }
   private createOrderTotals3(element) {
    return {
      "independent": element["RTOT3"],
      "autoadj": element["RTOT3A"],
      "finaladj": element["RTOT3F"],
      "totals":'',
      
    }
  }
  private createOrderTotals4(element) {
    return {
      "independent": element["RTOT4"],
      "autoadj": element["RTOT4A"],
      "finaladj": element["RTOT4F"],
      "totals":'Volume',
      
    }
  }
  private createOrderTotals5(element) {
    return {
      "independent": element["RTOT5"],
      "autoadj": element["RTOT5A"],
      "finaladj": element["RTOT5F"],
      "totals":'Dozens',
      
    }
  }
  private createOrderTotals6(element) {
    return {
      "independent": element["RTOT6"],
      "autoadj": element["RTOT6A"],
      "finaladj": element["RTOT6F"],
      "totals":'Cases',
      
    }
  }
  private createOrderTotals7(element) {
    return {
      "independent": element["RTOT7"],
      "autoadj": element["RTOT7A"],
      "finaladj": element["RTOT7F"],
      "totals":'Layers',
      
    }
  }
  private createOrderTotals8(element) {
    return {
      "independent": element["RTOT8"],
      "autoadj": element["RTOT8A"],
      "finaladj": element["RTOT8F"],
      "totals":'Pallets',
      
    }
  }
  private createOrderTotals9(element) {
    return {
      "independent": element["RTOT9"],
      "autoadj": element["RTOT9A"],
      "finaladj": element["RTOT9F"],
      "totals":'Others',
      
    }
  }
  
    convertAmountToNumeric(){
    this.dueOrderDetailsData["RTOT1"]=Number.parseInt(String(this.dueOrderDetailsData["RTOT1"]).slice(0,-4));
    this.dueOrderDetailsData["RTOT2"]=Number.parseInt(String(this.dueOrderDetailsData["RTOT2"]).slice(0,-4));
    this.dueOrderDetailsData["RTOT3"]=Number.parseInt(String(this.dueOrderDetailsData["RTOT3"]).slice(0,-4));
    this.dueOrderDetailsData["RTOT4"]=Number.parseInt(String(this.dueOrderDetailsData["RTOT4"]).slice(0,-4));
    this.dueOrderDetailsData["RTOT5"]=Number.parseInt(String(this.dueOrderDetailsData["RTOT5"]).slice(0,-4));
    this.dueOrderDetailsData["RTOT6"]=Number.parseInt(String(this.dueOrderDetailsData["RTOT6"]).slice(0,-4));
    this.dueOrderDetailsData["RTOT7"]=Number.parseInt(String(this.dueOrderDetailsData["RTOT7"]).slice(0,-4));
    this.dueOrderDetailsData["RTOT8"]=Number.parseInt(String(this.dueOrderDetailsData["RTOT8"]).slice(0,-4));
    this.dueOrderDetailsData["RTOT9"]=Number.parseInt(String(this.dueOrderDetailsData["RTOT9"]).slice(0,-4));

    this.dueOrderDetailsData["RTOT1A"]=Number.parseInt(String(this.dueOrderDetailsData["RTOT1A"]).slice(0,-4));
    this.dueOrderDetailsData["RTOT2A"]=Number.parseInt(String(this.dueOrderDetailsData["RTOT2A"]).slice(0,-4));
    this.dueOrderDetailsData["RTOT3A"]=Number.parseInt(String(this.dueOrderDetailsData["RTOT3A"]).slice(0,-4));
    this.dueOrderDetailsData["RTOT4A"]=Number.parseInt(String(this.dueOrderDetailsData["RTOT4A"]).slice(0,-4));
    this.dueOrderDetailsData["RTOT5A"]=Number.parseInt(String(this.dueOrderDetailsData["RTOT5A"]).slice(0,-4));
    this.dueOrderDetailsData["RTOT6A"]=Number.parseInt(String(this.dueOrderDetailsData["RTOT6A"]).slice(0,-4));
    this.dueOrderDetailsData["RTOT7A"]=Number.parseInt(String(this.dueOrderDetailsData["RTOT7A"]).slice(0,-4));
    this.dueOrderDetailsData["RTOT8A"]=Number.parseInt(String(this.dueOrderDetailsData["RTOT8A"]).slice(0,-4));
    this.dueOrderDetailsData["RTOT9A"]=Number.parseInt(String(this.dueOrderDetailsData["RTOT9A"]).slice(0,-4));

    this.dueOrderDetailsData["RTOT1F"]=Number.parseInt(String(this.dueOrderDetailsData["RTOT1F"]).slice(0,-4));
    this.dueOrderDetailsData["RTOT2F"]=Number.parseInt(String(this.dueOrderDetailsData["RTOT2F"]).slice(0,-4));
    this.dueOrderDetailsData["RTOT3F"]=Number.parseInt(String(this.dueOrderDetailsData["RTOT3F"]).slice(0,-4));
    this.dueOrderDetailsData["RTOT4F"]=Number.parseInt(String(this.dueOrderDetailsData["RTOT4F"]).slice(0,-4));
    this.dueOrderDetailsData["RTOT5F"]=Number.parseInt(String(this.dueOrderDetailsData["RTOT5F"]).slice(0,-4));
    this.dueOrderDetailsData["RTOT6F"]=Number.parseInt(String(this.dueOrderDetailsData["RTOT6F"]).slice(0,-4));
    this.dueOrderDetailsData["RTOT7F"]=Number.parseInt(String(this.dueOrderDetailsData["RTOT7F"]).slice(0,-4));
    this.dueOrderDetailsData["RTOT8F"]=Number.parseInt(String(this.dueOrderDetailsData["RTOT8F"]).slice(0,-4));
    this.dueOrderDetailsData["RTOT9F"]=Number.parseInt(String(this.dueOrderDetailsData["RTOT9F"]).slice(0,-4));
  }

  }
  

  
