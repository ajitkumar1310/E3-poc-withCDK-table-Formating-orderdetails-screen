import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OrderTotalTableComponentComponent } from './order-total-table-component.component';

describe('OrderTotalTableComponentComponent', () => {
  let component: OrderTotalTableComponentComponent;
  let fixture: ComponentFixture<OrderTotalTableComponentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OrderTotalTableComponentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrderTotalTableComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
