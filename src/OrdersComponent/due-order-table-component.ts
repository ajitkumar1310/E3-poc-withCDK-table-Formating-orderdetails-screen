import { Component, ElementRef, ViewChild, Input, Output, EventEmitter } from '@angular/core';
import { DataSource } from '@angular/cdk/table';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Observable } from 'rxjs/Observable';
import { Http, Response } from '@angular/http';
import { Headers, RequestOptions } from '@angular/http';
import { Router, ActivatedRoute, Params} from '@angular/router';

import 'rxjs/add/operator/startWith';
import 'rxjs/add/observable/merge';
import 'rxjs/add/operator/map';

@Component({
  selector: 'due-order-table',
  styleUrls: ['due-order-table-component.css'],
  templateUrl: 'due-order-table-component.html',
})
export class DueOrderTableComponent {
  dueOrderListDb = new DueOrderListDb(this._http);
  dataSource: OrderDataSource | null;
  ordersChartClicked:boolean=false;
  @Output() tableIntialized = new EventEmitter();
  @Input() orderChartIndex:number;
  @ViewChild('filter') filter: ElementRef;
 
  /** Table columns */
  columns = [
    { columnDef: 'Company', header: 'Company', cell: (row: IOrderData) => `${row.company}` },
    { columnDef: 'Warehouse', header: 'Warehouse', cell: (row: IOrderData) => `${row.warehouse}` },
    { columnDef: 'Vendor', header: 'Vendor', cell: (row: IOrderData) => `${row.vendor}` },
    { columnDef: 'SubVendor', header: 'SubVendor', cell: (row: IOrderData) => `${row.subvendor}` },
    { columnDef: 'Vendor Name', header: 'Vendor Name', cell: (row: IOrderData) => `${row.vendorname}` },
    { columnDef: 'Buyer Id', header: 'Buyer Id', cell: (row: IOrderData) => `${row.buyerid}` },
    { columnDef: 'Sup Vendor ID', header: 'Sup Vendor ID', cell: (row: IOrderData) => `${row.supvendor}` },
    { columnDef: 'Region', header: 'Region', cell: (row: IOrderData) => `${row.region}` },
    { columnDef: 'Amount', header: 'Amount', cell: (row: IOrderData) => `${row.amount}` },
    { columnDef: 'Eaches', header: 'Eaches', cell: (row: IOrderData) => `${row.eaches}` },
    { columnDef: 'Pallets', header: 'Pallets', cell: (row: IOrderData) => `${row.pallets}` }
  ];

  
  /** Column definitions in order */
 
  displayedColumns = this.columns.map(x => x.columnDef);
  
  handleRowClick(row){
    
    let supvndr:string=row.supvendor;
    let subvndr:string=row.subvendor;
    let vndr:string=row.vendor;
    let whse:string=row.warehouse;
      console.log(supvndr.trim().length);
    this.router.navigate(['/dueorders'], { queryParams: { whse:whse,vndr:vndr.toString().trim(),supv:supvndr.trim(),subv:subvndr.trim() } });
  }

  OrdersChartClicked(event){
    console.log('chart clicked'+'  '+event);
  }

  onOrdersChartClicked(n:number) {
    console.log('Event Fired'+' '+n);
    
    this.dueOrderListDb.addOrder(n);
    this.ordersChartClicked=true;
    
  }
   constructor(private _http: Http,private router: Router) {
    //this.addOrder();
    this.tableIntialized.emit();
  }

  ngOnInit() {
    this.dataSource = new OrderDataSource(this.dueOrderListDb);
   
    //filter
    Observable.fromEvent(this.filter.nativeElement, 'keyup')
      .debounceTime(150)
      .distinctUntilChanged()
      .subscribe(() => {
        if (!this.dataSource) { return; }
        this.dataSource.filter = this.filter.nativeElement.value;
      });
      
  }
}
export interface IOrderData {
  company: string,
  warehouse: string,
  vendor: string,
  supvendor: string,
  vendorname: string,
  subvendor: string,
  buyerid: string,
  region: string,
  eaches: number,
  pallets: number,
  amount: number
}
export class OrderData implements IOrderData {
  company: string;
  warehouse: string;
  vendor: string;
  supvendor: string;
  vendorname: string;
  subvendor: string;
  buyerid: string;
  region: string;
  eaches: number;
  pallets: number;
  amount: number;
}
// Order Data source for Table
export class OrderDataSource extends DataSource<any> {
  _filterChange = new BehaviorSubject('');
  get filter(): string { return this._filterChange.value; }
  set filter(filter: string) { this._filterChange.next(filter); }
  connect(): Observable<any[]> {
    const displayDataChanges = [
      this._dueOrderListDb.dataChange,
      this._filterChange,
    ];
    return Observable.merge(...displayDataChanges).map(() => {
      return this._dueOrderListDb.data.slice().filter((item: OrderData) => {
        let searchStr = (item.vendor).toLowerCase();
        return searchStr.indexOf(this.filter.toLowerCase()) != -1;
      });
    });
  }
  disconnect(): void {

  }
  constructor(private _dueOrderListDb: DueOrderListDb) {
    super();
  }
}

export class DueOrderListDb {
  @Input() ordersChartIndex:number;
  @Input() expediteChartIndex:number;

  private ordersChartUrl="http://e3cdd.jda.com:10000/web/services/AWR10/BUYERS/001?TYPE=DETAIL&ACTION=DETAIL_VIEW&003303=E3T&003174=JDA&3161=910";
  private dueOrdersListUrl="http://e3cdd.jda.com:10000/web/services/AWR10/ORDERS?TYPE=LIST&ACTION=LISTVIEW&PAGE=F&003303=E3T&003174=JDA&004261001=B&3161=910";
  private forwardBuysListUrl="http://e3cdd.jda.com:10000/web/services/AWR10/ORDERS?TYPE=LIST&ACTION=LISTVIEW&PAGE=F&003303=E3T&003174=JDA&&3161=910&004261001=C";
  private plannedListUrl="http://e3cdd.jda.com:10000/web/services/AWR10/ORDERS?TYPE=LIST&ACTION=LISTVIEW&PAGE=F&003303=E3T&003174=JDA&&3161=910&004261001=D";
  private bookedListUrl="http://e3cdd.jda.com:10000/web/services/AWR10/ORDERS?TYPE=LIST&ACTION=LISTVIEW&PAGE=F&003303=E3T&003174=JDA&&3161=910&004261001=E";
  private transferListUrl="http://e3cdd.jda.com:10000/web/services/AWR10/ORDERS?TYPE=LIST&ACTION=LISTVIEW&PAGE=F&003303=E3T&003174=JDA&3161=910&004261001=F";
  private backOrderListUrl="http://e3cdd.jda.com:10000/web/services/AWR10/ORDERS?TYPE=LIST&ACTION=LISTVIEW&PAGE=F&003303=E3T&003174=JDA&3161=910&004261001=G";
  private alterOrdersListUrl="http://e3cdd.jda.com:10000/web/services/AWR10/ORDERS?TYPE=LIST&ACTION=LISTVIEW&PAGE=F&003303=E3T&003174=JDA&&3161=910&004263001<=1&004261001<>Z";
  data1:any=null;
  errorMessage:string;
  dataChange: BehaviorSubject<OrderData[]> = new BehaviorSubject<OrderData[]>([]);
  get data(): OrderData[] { return this.dataChange.value }
  
  constructor(private http: Http) {
    //this.addOrder();
     
  }

  
  public addOrder(n:number) {
    console.log(this.ordersChartIndex);
    let url:string;
    url=this.getOrdersChartUrl(n);
    let headers = new Headers({ 'Content-Type': 'application/json' });   
    let dueOrdersList = [];
    let options = new RequestOptions({ headers: headers });
    this.data1=[{"RRECD":"006000","RSTAT":" ","RCOMP":"E3T","RWHSE":"910","RVNDR":"1050    ","RSUBV":"    ","RWGRP":" ","RTRUK":"000","RSUPV":"        ","RBUYR":"JDA","RVNAME":"VENDOR 001050                 ","RREGON":"   ","RSEQ#":"                  ","ROTYP":"B","RSTUS":"1","RBRKT":"0","RODEL":"000","RTOT1F":"00000001110972000","RTOT2F":"0000014380000","RTOT3F":"0000000000000","RTOT4F":"0000014380000","RTOT5F":"0000001198337","RTOT6F":"0000013930000","RTOT7F":"0000013930000","RTOT8F":"0000013930000","RTOT9F":"0000014380000","VGRP1":"   ","VGRP2":"   ","VGRP3":"   ","VGRP4":"   ","VGRP5":"   ","VGRP6":"   ","VOPTD3":"0","VCURCD":"   ","VMPRNT":"1","RPINM":"0000000","RXDRT":"000","RGCNT":"00000","VFCSTD":"       ","VORMON":"00","VORWEK":"0","VNXTDT":"0000000","WREGON":"   ","RDLSTS":"0"},{"RRECD":"006000","RSTAT":" ","RCOMP":"E3T","RWHSE":"910","RVNDR":"1050    ","RSUBV":"EE  ","RWGRP":" ","RTRUK":"000","RSUPV":"        ","RBUYR":"JDA","RVNAME":"VENDOR 001050                 ","RREGON":"   ","RSEQ#":"                  ","ROTYP":"B","RSTUS":"1","RBRKT":"0","RODEL":"000","RTOT1F":"00000000444282000","RTOT2F":"0000011350000","RTOT3F":"0000000000000","RTOT4F":"0000011350000","RTOT5F":"0000000945834","RTOT6F":"0000010900000","RTOT7F":"0000010900000","RTOT8F":"0000010900000","RTOT9F":"0000011350000","VGRP1":"   ","VGRP2":"   ","VGRP3":"   ","VGRP4":"   ","VGRP5":"   ","VGRP6":"   ","VOPTD3":"0","VCURCD":"   ","VMPRNT":"0","RPINM":"3719341","RXDRT":"000","RGCNT":"00000","VFCSTD":"       ","VORMON":"00","VORWEK":"0","VNXTDT":"0000000","WREGON":"   ","RDLSTS":"0"},{"RRECD":"006000","RSTAT":" ","RCOMP":"E3T","RWHSE":"910","RVNDR":"1050    ","RSUBV":"EG  ","RWGRP":" ","RTRUK":"000","RSUPV":"        ","RBUYR":"JDA","RVNAME":"VENDOR 001050                 ","RREGON":"   ","RSEQ#":"                  ","ROTYP":"B","RSTUS":"1","RBRKT":"0","RODEL":"000","RTOT1F":"00000000666690000","RTOT2F":"0000003030000","RTOT3F":"0000000000000","RTOT4F":"0000003030000","RTOT5F":"0000000252503","RTOT6F":"0000003030000","RTOT7F":"0000003030000","RTOT8F":"0000003030000","RTOT9F":"0000003030000","VGRP1":"   ","VGRP2":"   ","VGRP3":"   ","VGRP4":"   ","VGRP5":"   ","VGRP6":"   ","VOPTD3":"0","VCURCD":"   ","VMPRNT":"0","RPINM":"0000000","RXDRT":"000","RGCNT":"00000","VFCSTD":"       ","VORMON":"00","VORWEK":"0","VNXTDT":"0000000","WREGON":"   ","RDLSTS":"0"},{"RRECD":"006000","RSTAT":" ","RCOMP":"E3T","RWHSE":"910","RVNDR":"1500    ","RSUBV":"    ","RWGRP":" ","RTRUK":"000","RSUPV":"        ","RBUYR":"JDA","RVNAME":"VENDOR 001500                 ","RREGON":"   ","RSEQ#":"                  ","ROTYP":"B","RSTUS":"1","RBRKT":"1","RODEL":"000","RTOT1F":"00000000160000000","RTOT2F":"0000020000000","RTOT3F":"0000000000000","RTOT4F":"0000020000000","RTOT5F":"0000001666668","RTOT6F":"0000020000000","RTOT7F":"0000020000000","RTOT8F":"0000020000000","RTOT9F":"0000020000000","VGRP1":"   ","VGRP2":"   ","VGRP3":"   ","VGRP4":"   ","VGRP5":"   ","VGRP6":"   ","VOPTD3":"0","VCURCD":"   ","VMPRNT":"0","RPINM":"0000000","RXDRT":"300","RGCNT":"00000","VFCSTD":"       ","VORMON":"00","VORWEK":"0","VNXTDT":"0000000","WREGON":"   ","RDLSTS":"0"},{"RRECD":"006000","RSTAT":" ","RCOMP":"E3T","RWHSE":"910","RVNDR":"5021    ","RSUBV":"A   ","RWGRP":" ","RTRUK":"000","RSUPV":"        ","RBUYR":"JDA","RVNAME":"VENDOR 005021                 ","RREGON":"   ","RSEQ#":"                  ","ROTYP":"B","RSTUS":"1","RBRKT":"0","RODEL":"000","RTOT1F":"00000000034070000","RTOT2F":"0000000520000","RTOT3F":"0000000000000","RTOT4F":"0000000520000","RTOT5F":"0000000043333","RTOT6F":"0000000520000","RTOT7F":"0000000520000","RTOT8F":"0000000520000","RTOT9F":"0000000520000","VGRP1":"   ","VGRP2":"   ","VGRP3":"   ","VGRP4":"   ","VGRP5":"   ","VGRP6":"   ","VOPTD3":"0","VCURCD":"   ","VMPRNT":"0","RPINM":"0000000","RXDRT":"000","RGCNT":"00000","VFCSTD":"       ","VORMON":"00","VORWEK":"0","VNXTDT":"0000000","WREGON":"   ","RDLSTS":"0"},{"RRECD":"006000","RSTAT":" ","RCOMP":"E3T","RWHSE":"910","RVNDR":"5086    ","RSUBV":"    ","RWGRP":" ","RTRUK":"000","RSUPV":"        ","RBUYR":"JDA","RVNAME":"VENDOR 005086                 ","RREGON":"   ","RSEQ#":"                  ","ROTYP":"B","RSTUS":"1","RBRKT":"0","RODEL":"000","RTOT1F":"00000013745313844","RTOT2F":"0000118780000","RTOT3F":"0000000000000","RTOT4F":"0000118780000","RTOT5F":"0000009898333","RTOT6F":"0000068730000","RTOT7F":"0000068730000","RTOT8F":"0000068730000","RTOT9F":"0000118780000","VGRP1":"   ","VGRP2":"   ","VGRP3":"   ","VGRP4":"   ","VGRP5":"   ","VGRP6":"   ","VOPTD3":"0","VCURCD":"   ","VMPRNT":"1","RPINM":"0000000","RXDRT":"000","RGCNT":"00000","VFCSTD":"       ","VORMON":"00","VORWEK":"0","VNXTDT":"0000000","WREGON":"   ","RDLSTS":"0"},{"RRECD":"006000","RSTAT":" ","RCOMP":"E3T","RWHSE":"910","RVNDR":"7800    ","RSUBV":"    ","RWGRP":" ","RTRUK":"000","RSUPV":"        ","RBUYR":"JDA","RVNAME":"VENDOR 007800                 ","RREGON":"   ","RSEQ#":"                  ","ROTYP":"B","RSTUS":"1","RBRKT":"0","RODEL":"000","RTOT1F":"00000001071478779","RTOT2F":"0000004530000","RTOT3F":"0000000000000","RTOT4F":"0000004530000","RTOT5F":"0000000377501","RTOT6F":"0000004530000","RTOT7F":"0000004530000","RTOT8F":"0000004530000","RTOT9F":"0000004530000","VGRP1":"   ","VGRP2":"   ","VGRP3":"   ","VGRP4":"   ","VGRP5":"   ","VGRP6":"   ","VOPTD3":"0","VCURCD":"   ","VMPRNT":"0","RPINM":"0000000","RXDRT":"000","RGCNT":"00000","VFCSTD":"       ","VORMON":"00","VORWEK":"0","VNXTDT":"0000000","WREGON":"   ","RDLSTS":"0"},{"RRECD":"006000","RSTAT":" ","RCOMP":"E3T","RWHSE":"910","RVNDR":"7800    ","RSUBV":"    ","RWGRP":" ","RTRUK":"000","RSUPV":"        ","RBUYR":"JDA","RVNAME":"VENDOR 007800                 ","RREGON":"   ","RSEQ#":"                  ","ROTYP":"B","RSTUS":"1","RBRKT":"0","RODEL":"000","RTOT1F":"00000001071478779","RTOT2F":"0000004530000","RTOT3F":"0000000000000","RTOT4F":"0000004530000","RTOT5F":"0000000377501","RTOT6F":"0000004530000","RTOT7F":"0000004530000","RTOT8F":"0000004530000","RTOT9F":"0000004530000","VGRP1":"   ","VGRP2":"   ","VGRP3":"   ","VGRP4":"   ","VGRP5":"   ","VGRP6":"   ","VOPTD3":"0","VCURCD":"   ","VMPRNT":"0","RPINM":"0000000","RXDRT":"000","RGCNT":"00000","VFCSTD":"       ","VORMON":"00","VORWEK":"0","VNXTDT":"0000000","WREGON":"   ","RDLSTS":"0"},{"RRECD":"006000","RSTAT":" ","RCOMP":"E3T","RWHSE":"910","RVNDR":"7800    ","RSUBV":"    ","RWGRP":" ","RTRUK":"000","RSUPV":"        ","RBUYR":"JDA","RVNAME":"VENDOR 007800                 ","RREGON":"   ","RSEQ#":"                  ","ROTYP":"B","RSTUS":"1","RBRKT":"0","RODEL":"000","RTOT1F":"00000001071478779","RTOT2F":"0000004530000","RTOT3F":"0000000000000","RTOT4F":"0000004530000","RTOT5F":"0000000377501","RTOT6F":"0000004530000","RTOT7F":"0000004530000","RTOT8F":"0000004530000","RTOT9F":"0000004530000","VGRP1":"   ","VGRP2":"   ","VGRP3":"   ","VGRP4":"   ","VGRP5":"   ","VGRP6":"   ","VOPTD3":"0","VCURCD":"   ","VMPRNT":"0","RPINM":"0000000","RXDRT":"000","RGCNT":"00000","VFCSTD":"       ","VORMON":"00","VORWEK":"0","VNXTDT":"0000000","WREGON":"   ","RDLSTS":"0"},{"RRECD":"006000","RSTAT":" ","RCOMP":"E3T","RWHSE":"910","RVNDR":"7800    ","RSUBV":"    ","RWGRP":" ","RTRUK":"000","RSUPV":"        ","RBUYR":"JDA","RVNAME":"VENDOR 007800                 ","RREGON":"   ","RSEQ#":"                  ","ROTYP":"B","RSTUS":"1","RBRKT":"0","RODEL":"000","RTOT1F":"00000001071478779","RTOT2F":"0000004530000","RTOT3F":"0000000000000","RTOT4F":"0000004530000","RTOT5F":"0000000377501","RTOT6F":"0000004530000","RTOT7F":"0000004530000","RTOT8F":"0000004530000","RTOT9F":"0000004530000","VGRP1":"   ","VGRP2":"   ","VGRP3":"   ","VGRP4":"   ","VGRP5":"   ","VGRP6":"   ","VOPTD3":"0","VCURCD":"   ","VMPRNT":"0","RPINM":"0000000","RXDRT":"000","RGCNT":"00000","VFCSTD":"       ","VORMON":"00","VORWEK":"0","VNXTDT":"0000000","WREGON":"   ","RDLSTS":"0"},{"RRECD":"006000","RSTAT":" ","RCOMP":"E3T","RWHSE":"910","RVNDR":"7800    ","RSUBV":"    ","RWGRP":" ","RTRUK":"000","RSUPV":"        ","RBUYR":"JDA","RVNAME":"VENDOR 007800                 ","RREGON":"   ","RSEQ#":"                  ","ROTYP":"B","RSTUS":"1","RBRKT":"0","RODEL":"000","RTOT1F":"00000001071478779","RTOT2F":"0000004530000","RTOT3F":"0000000000000","RTOT4F":"0000004530000","RTOT5F":"0000000377501","RTOT6F":"0000004530000","RTOT7F":"0000004530000","RTOT8F":"0000004530000","RTOT9F":"0000004530000","VGRP1":"   ","VGRP2":"   ","VGRP3":"   ","VGRP4":"   ","VGRP5":"   ","VGRP6":"   ","VOPTD3":"0","VCURCD":"   ","VMPRNT":"0","RPINM":"0000000","RXDRT":"000","RGCNT":"00000","VFCSTD":"       ","VORMON":"00","VORWEK":"0","VNXTDT":"0000000","WREGON":"   ","RDLSTS":"0"},{"RRECD":"006000","RSTAT":" ","RCOMP":"E3T","RWHSE":"910","RVNDR":"7800    ","RSUBV":"    ","RWGRP":" ","RTRUK":"000","RSUPV":"        ","RBUYR":"JDA","RVNAME":"VENDOR 007800                 ","RREGON":"   ","RSEQ#":"                  ","ROTYP":"B","RSTUS":"1","RBRKT":"0","RODEL":"000","RTOT1F":"00000001071478779","RTOT2F":"0000004530000","RTOT3F":"0000000000000","RTOT4F":"0000004530000","RTOT5F":"0000000377501","RTOT6F":"0000004530000","RTOT7F":"0000004530000","RTOT8F":"0000004530000","RTOT9F":"0000004530000","VGRP1":"   ","VGRP2":"   ","VGRP3":"   ","VGRP4":"   ","VGRP5":"   ","VGRP6":"   ","VOPTD3":"0","VCURCD":"   ","VMPRNT":"0","RPINM":"0000000","RXDRT":"000","RGCNT":"00000","VFCSTD":"       ","VORMON":"00","VORWEK":"0","VNXTDT":"0000000","WREGON":"   ","RDLSTS":"0"}];
    for(let element of this.data1){              
        console.log(element);
        if(element){
           let jsonData = element; 
           jsonData["RTOT1F"]=Number.parseFloat(String(jsonData["RTOT1F"]).slice(0,-4));
           jsonData["RTOT2F"]=Number.parseInt(String(jsonData["RTOT2F"]).slice(0,-4));
           jsonData["RTOT8F"]=Number.parseInt(String(jsonData["RTOT8F"]).slice(0,-4));
           dueOrdersList.push(this.createOrder(jsonData));  
          }                              
    }      
      
      this.dataChange.next(dueOrdersList);
    /* Commented for Ram this.http.get(url,options).map(res=>res.json()).subscribe((data) => {   
    this.data1=data.JASONSTR;  
    for(let element of this.data1){              
        console.log(element);
        if(element){
           let jsonData = JSON.parse(element); 
           jsonData["RTOT1F"]=Number.parseFloat(String(jsonData["RTOT1F"]).slice(0,-4));
           jsonData["RTOT2F"]=Number.parseInt(String(jsonData["RTOT2F"]).slice(0,-4));
           jsonData["RTOT8F"]=Number.parseInt(String(jsonData["RTOT8F"]).slice(0,-4));
           dueOrdersList.push(this.createOrder(jsonData));  
          }                              
    }      
      
      this.dataChange.next(dueOrdersList);
      },(error: any) => this.errorMessage = <any>error);*/
      
    }

    getOrdersChartUrl(n:Number){
      let url;
      switch(n){
        case 0:url= this.dueOrdersListUrl;break;
        case 1:url= this.alterOrdersListUrl;break;
        case 2:url= this.forwardBuysListUrl;break;
        case 3:url= this.plannedListUrl;break;
        case 4:url= this.bookedListUrl;break;
        case 5:url= this.transferListUrl;break;
        case 6:url= this.backOrderListUrl;break;

      }
        return url;
    }
    private createOrder(element) {
    return {
      "warehouse": element["RWHSE"],
      "company": element["RCOMP"],
      "vendor": element["RVNDR"],
      "subvendor": element["RSUBV"],
      "vendorname": element["RVNAME"],
      "supvendor": element["RSUPV"],
      "buyerid": element["RBUYR"],
      "region": element["RREGON"],
      "amount": Number.parseFloat(element["RTOT1F"]),
      "eaches": Number.parseFloat(element["RTOT2F"]),
      "pallets": Number.parseFloat(element["RTOT8F"])
    }
  }
  

  
}