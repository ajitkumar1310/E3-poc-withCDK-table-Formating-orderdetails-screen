import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DueOrderTableComponent } from './due-order-table-component';

describe('TableBasicExampleComponent', () => {
  let component: DueOrderTableComponent;
  let fixture: ComponentFixture<DueOrderTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DueOrderTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DueOrderTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
