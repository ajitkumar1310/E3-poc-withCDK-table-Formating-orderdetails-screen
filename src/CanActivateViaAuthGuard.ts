import { Injectable } from '@angular/core';
import {Router, CanActivate } from '@angular/router';
import { AuthenticationService } from './Services/authentication.service';

@Injectable()
export class CanActivateViaAuthGuard implements CanActivate {

  constructor(private authService: AuthenticationService,private router: Router) {
      console.log('in authguard');
  }

  canActivate() {
      if(this.authService.isLoggedIn()){
          return true;
      }
      this.router.navigate(['login']);
    return false;
   // return this.authService.isLoggedIn();
  }
}