import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { HttpModule } from '@angular/http';
import { AppComponent } from './app.component';
import { FormsModule,ReactiveFormsModule  }   from '@angular/forms';
import {LoginComponent} from '../Login/LoginComponent';
import {HomeComponent} from '../home/home.component';
import { AuthenticationService } from '../Services/authentication.service';
import {CanActivateViaAuthGuard} from '../CanActivateViaAuthGuard';
import { MdButtonModule, MdCheckboxModule, MdTableModule, MdInputModule, MaterialModule, MdDatepickerModule, MdNativeDateModule } from '@angular/material';
import { OrdersComponentComponent } from '../OrdersComponent/orders-component.component';
import {FlexLayoutModule} from "@angular/flex-layout";
import { CommonModule } from '@angular/common';
import { ChartModule } from 'angular2-highcharts';
import { HighchartsStatic } from 'angular2-highcharts/dist/HighchartsService';
import {SlimLoadingBarModule} from 'ng2-slim-loading-bar';
import {OrderListFilterPipe } from '../Filter/list.filter';
import {DueorderDetailComponent} from '../OrdersComponent/dueorder-detail-component.component'
import { BrowserAnimationsModule } from '@angular/platform-browser/animations'; 
import {NgxDatatableModule} from '@swimlane/ngx-datatable';
import {CdkTableModule} from '@angular/cdk/table';
import { DueOrderTableComponent } from '../OrdersComponent/due-order-table-component';
import { OrderTotalTableComponentComponent } from '../OrdersComponent/order-total-table-component/order-total-table-component.component';

//import { HighchartsStatic } from 'angular2-highcharts/dist/HighchartsService';
declare var require: any;
/*export function highchartsFactory() {
  return require('highcharts');
}*/
export function highchartsFactory() {
const hc = require('highcharts/highstock');
const dd = require('highcharts/modules/exporting');
dd(hc);
return hc;
}
@NgModule({
  declarations: [
    AppComponent,LoginComponent,HomeComponent, 
    OrdersComponentComponent,OrderListFilterPipe,
    DueorderDetailComponent,
    DueOrderTableComponent,
    OrderTotalTableComponentComponent
  ],
  imports: [
    BrowserModule,FormsModule,HttpModule,ReactiveFormsModule,NgxDatatableModule,MaterialModule,
    CommonModule,CdkTableModule, MdButtonModule, MdCheckboxModule, CdkTableModule, 
    MdTableModule, MdInputModule,BrowserAnimationsModule,MdDatepickerModule,MdNativeDateModule,
    RouterModule.forRoot([
     { path: 'login',  component: LoginComponent },
     { path: 'home',  component: HomeComponent },
     { path: 'dueorders', component:DueorderDetailComponent }
    ]),FlexLayoutModule,ChartModule/*.forRoot(require('highcharts'))*/,SlimLoadingBarModule.forRoot()
  ],
  providers: [AuthenticationService,CanActivateViaAuthGuard,{
provide: HighchartsStatic,
useFactory: highchartsFactory
}],
  bootstrap: [AppComponent]
})
export class AppModule {

 }

