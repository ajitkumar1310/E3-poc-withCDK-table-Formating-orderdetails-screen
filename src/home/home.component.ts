import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from '../Services/authentication.service';

//import { MaterialModule } from '@angular/material';
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  title = 'app';
  isLoggedin:boolean;
  userName:string;
  constructor( private _authservice:AuthenticationService){
    this.isLoggedin=this._authservice.isLoggedIn();
    if( this.isLoggedin){
      this.userName=this._authservice.getUsername();
    }
  }

  LogOut(){
    this._authservice.logout();
  }

  ngOnInit() {
  }

}
