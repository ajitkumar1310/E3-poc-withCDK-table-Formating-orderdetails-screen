import {Injectable} from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs/Subscription';
import { LoginFormObject} from '../Login/LoginFormObject';
import {LoginResult} from '../Login/Results';
import { Headers, RequestOptions } from '@angular/http';
import { Http, Response } from '@angular/http';
import { FormGroup, FormBuilder, Validators, AbstractControl, ValidatorFn, FormArray } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';

export class User {
  constructor(
    public email: string,
    public password: string) { }
}
 
var users = [
  new User('admin@admin.com','adm9'),
  new User('user1@gmail.com','a23')
];
 
@Injectable()
export class AuthenticationService {
  errorMessage: string;
  
  url=`http://e3cdd.jda.com:10010/web/services/LOGIN`;
    public errorMsg = '';
   
   returnUrl:string;
    loginForm: FormGroup;
    currentUser:string;
  logout() {
    localStorage.removeItem(this.currentUser);
    this.router.navigate(['Login']);
  }

  constructor(private fb: FormBuilder,private route: ActivatedRoute,
        private router: Router,private http: Http) {           

        }
 
  login(user){
    var authenticatedUser = users.find(u => u.email === user.email);
    if (authenticatedUser && authenticatedUser.password === user.password){
     // localStorage.setItem("user", authenticatedUser);
      this.router.navigate(['Home']);      
      return true;
    }
    return false;
 
  }
  LoginWithObservable(loginobject:LoginFormObject): Observable<LoginResult> {
	    let headers = new Headers({ 'Content-Type': 'application/json' });   
       
      let options = new RequestOptions({ headers: headers });
       
      this.returnUrl='Ajit success11'
      this.currentUser=loginobject.userName;
      return this.http.post(this.url,JSON.stringify(loginobject), options)
                   .map(this.extractData);
                  // .catch(this.handleError);
    }
    
    private handleError(error: Response): Observable<any> {
        // in a real world app, we may send the server to some remote logging infrastructure
        // instead of just logging it to the console
        console.log('Failed');
         console.log(error.json().error);
        console.error(error.json().error);
        this.errorMsg='Failed to Login';
        return Observable.throw(error.json().error || 'Server error');
    }
    private extractData(response: Response) {
        console.log('sucess');
        localStorage.setItem(this.currentUser, 'loggedin');
        return {};
    }
 
  
    isLoggedIn():boolean{
      if(localStorage.getItem(this.currentUser)==null){
        return false;
      }
      else{
        return true;
      }

    }
    
    getUsername=()=>{
      console.log(this.returnUrl);
      return this.currentUser;
    }
    
   
}