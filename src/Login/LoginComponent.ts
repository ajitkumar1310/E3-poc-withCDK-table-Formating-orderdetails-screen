import {Component,OnInit, ElementRef} from '@angular/core';
import {AuthenticationService, User} from '../Services/authentication.service';
import { Http, Response } from '@angular/http';
import { Headers, RequestOptions } from '@angular/http';
import { FormGroup, FormBuilder, Validators, AbstractControl, ValidatorFn, FormArray } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
 
 import {LoginResult} from './Results';
 import { LoginFormObject} from './LoginFormObject';
import 'rxjs/add/observable/fromEvent';
import 'rxjs/add/observable/merge';
import 'rxjs/add/operator/debounceTime';
import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs/Subscription';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/of';


@Component({
    selector: 'login-form',
    providers: [AuthenticationService],
    templateUrl: 'LoginComponent.html'
})
 
export class LoginComponent {
    returnUrl: string;
    loginobject:LoginFormObject;
    loginForm: FormGroup;
    public errorMsg = '';
    errorMessage: string;
    ngOnInit() {
        
 
        // get return url from route parameters or default to '/'
        this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
        this.errorMsg='';
    }
    constructor(private fb: FormBuilder,private route: ActivatedRoute,
        private router: Router, private _authservice:AuthenticationService,private http: Http) {
            this.loginForm = this.fb.group({
                     'userName':[''],
                     'passWord':['']
            
             });

        }

       
 
    login() {
        console.log(this.loginForm.get('userName').value);
        this.loginobject=new LoginFormObject();
        this.loginobject.userName=this.loginForm.get('userName').value;
        this.loginobject.passWord=this.loginForm.get('passWord').value;
        let p=Object.assign({},this.loginobject,this.loginForm.value)
        this._authservice.LoginWithObservable(p).subscribe(() => this.onSaveComplete(),
         (error: any) => this.errorMessage = <any>error);
         console.log('Login Failed');
        //console.log(this.loginForm.get('userName').value);
        this.errorMsg='Login Failed';
          
        
    }
     onSaveComplete(): void {
         console.log('Successfully logged in');
          this.router.navigate(['home']);  
     }
    
}