import {Pipe,PipeTransform} from '@angular/core';
import {IOrder} from '../OrdersComponent/order';
 


@Pipe({
    name:'ProductFilter'
})

export class OrderListFilterPipe implements PipeTransform{

    transform(value:IOrder[],filterby:string):IOrder[]{

        filterby=filterby?filterby.toLocaleLowerCase():null;

        return filterby ? value.filter((order:IOrder)=>
        order.RVNDR.toLocaleLowerCase().indexOf(filterby)!==-1):value;
    }
}